#!/bin/bash
echo -e "\e[32mInformation general du raspberry"
echo -e "\e[31mDate systeme :\e[0m"
date
echo -e "\e[31mTemps depuis demarrage :\e[0m"
uptime
echo -e "\e[31mTemperature :\e[0m"
/opt/vc/bin/vcgencmd measure_temp
echo -e "\e[31mCarte memoire :\e[0m"
df -k /
echo -e "\e[31mCpu usage (%):\e[0m"
top -bn 1 | awk '{print $9}' | tail -n +8 | awk '{s+=$1} END {print s }' 
echo -e "\e[31mRAM usage (%):\e[0m"
top -bn 1 | awk '{print $10}' | tail -n +8 | awk '{s+=$1} END {print s }' 
exit 0