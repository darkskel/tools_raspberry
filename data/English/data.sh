#!/bin/bash
echo -e "\e[32mGeneral Data of raspberry"
echo -e "\e[31mDate system :\e[0m"
date
echo -e "\e[31mTime since boot :\e[0m"
uptime
echo -e "\e[31mTemperature :\e[0m"
/opt/vc/bin/vcgencmd measure_temp
echo -e "\e[31mSd Card :\e[0m"
df -k /
echo -e "\e[31mCpu use (%):\e[0m"
top -bn 1 | awk '{print $9}' | tail -n +8 | awk '{s+=$1} END {print s }' 
echo -e "\e[31mRAM use (%):\e[0m"
top -bn 1 | awk '{print $10}' | tail -n +8 | awk '{s+=$1} END {print s }' 
exit 0